var isAdmin = false;

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
  var authentication = document.getElementById("authentication");
  authentication.innerHTML = '<th id="authentication"><button onclick="signOut();" type="button" class="btn btn-info">Sign Out</button></th>';
  checkAdmin(profile)
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    }); 
	auth2.disconnect();
	
	var authentication = document.getElementById("authentication");
	authentication.innerHTML = '<th id="authentication"><div class="g-signin2" data-onsuccess="onSignIn"></div></th>';
	var element = document.getElementById("adminOptions");
	element.style.visibility = "hidden";
	
	setTimeout(function(){
		window.location.reload(); },100);
}

function checkAdmin(profile) {
	if(profile.getId() == 106765494108169919533) {
		var element = document.getElementById("adminOptions");
		element.style.visibility = "visible"
	}
}