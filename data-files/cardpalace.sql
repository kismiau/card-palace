-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2018 at 04:35 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cardpalace`
--

-- --------------------------------------------------------

--
-- Table structure for table `boxbreak`
--

CREATE TABLE `boxbreak` (
  `itemID` int(8) NOT NULL,
  `teamID` int(8) NOT NULL,
  `price` double(8,2) NOT NULL,
  `customerID` int(8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `categoryID` int(8) NOT NULL,
  `category` varchar(72) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryID`, `category`) VALUES
(1, 'Comic Book'),
(2, 'Yu-Gi-Oh! Singles'),
(3, 'Pokemon Singles'),
(4, 'Magic the Gathering Singles'),
(5, 'Board Games');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customerID` int(8) NOT NULL,
  `fName` varchar(36) NOT NULL,
  `lName` varchar(36) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `country` varchar(36) NOT NULL,
  `state` varchar(36) NOT NULL,
  `city` varchar(36) NOT NULL,
  `street` varchar(36) NOT NULL,
  `zipcode` varchar(6) NOT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `itemID` int(8) NOT NULL,
  `name` varchar(72) NOT NULL,
  `categoryID` int(8) NOT NULL,
  `details` text NOT NULL,
  `price` double(8,2) NOT NULL,
  `quantity` int(2) NOT NULL,
  `imageID` int(8) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemID`, `name`, `categoryID`, `details`, `price`, `quantity`, `imageID`, `dateAdded`, `deleted`) VALUES
(12, 'Red-Eyes B. Dragon', 2, 'A ferocious dragon with a deadly attack.', 1.77, 9, 14, '2018-05-11 08:46:33', 0),
(13, 'Blue-Eyes White Dragon', 2, 'This legendary dragon is a powerful engine of destruction. Virtually invincible, very few have faced this awesome creature and lived to tell the tale.', 1.21, 6, 15, '2018-05-11 08:46:33', 0),
(14, 'Dark Magician', 2, 'The ultimate wizard in terms of attack and defense.', 2.03, 5, 16, '2018-05-11 08:46:33', 0),
(15, 'Charizard', 3, 'X/Y Evolutions', 6.00, 6, 17, '2018-05-11 08:46:33', 0),
(16, 'Blastoise', 3, 'Base Set 2', 6.76, 2, 18, '2018-05-11 08:46:33', 0),
(17, 'Venusaur', 3, 'Base Set', 4.35, 4, 19, '2018-05-11 08:46:33', 0),
(18, 'Black Lotus', 4, '{T}, Sacrifice Black Lotus: Add three mana of any one color to your mana pool.', 13628.91, 1, 22, '2018-05-11 08:46:33', 0),
(19, 'Ulamog, the Infinite Gyre', 4, 'When you cast Ulamog, the Infinite Gyre, destroy target permanent. Annihilator 4 (Whenever this creature attacks, defending player sacrifices four permanents.) Ulamog is indestructible. When Ulamog is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.', 77.21, 2, 21, '2018-05-11 08:46:33', 0),
(20, 'Jace, the Mind Sculptor', 4, '+2: Look at the top card of target player\'s library. You may put that card on the bottom of that player\'s library. 0: Draw three cards, then put two cards from your hand on top of your library in any order. -1: Return target creature to its owner\'s hand. -12: Exile all cards from target player\'s library, then that player shuffles his or her hand into his or her library.', 72.34, 8, 20, '2018-05-11 08:46:33', 0),
(21, 'Amazing Spider-Man #375', 1, 'Issue Condition: NEAR MINT', 6.00, 1, 24, '2018-05-11 08:46:33', 0),
(22, 'Doctor Strange #46', 1, 'Issue Condition: NEAR MINT', 5.00, 1, 25, '2018-05-11 08:46:33', 0),
(23, 'Thor #340', 1, 'Issue Condition: NEAR MINT', 3.00, 1, 23, '2018-05-11 08:46:33', 0),
(24, 'Clue', 5, 'The classic detective game! In Clue, players move from room to room in a mansion to solve the mystery of: who done it, with what, and where? Players are dealt character, weapon, and location cards after the top card from each card type is secretly placed in the confidential file in the middle of the board. Players must move to a room and then make an accusation against a character saying they did it in that room with a specific weapon. The player to the left must show one of any cards accused to the accuser if in that player\'s hand. Through deductive reasoning each player must figure out which character, weapon, and location are in the secret file. To do this, each player must uncover what cards are in other players hands by making more and more accusations. Once a player knows what cards the other players are holding, they will know what cards are in the secret file. A great game for those who enjoy reasoning and thinking things out.', 22.99, 6, 28, '2018-05-11 08:46:33', 0),
(25, 'Monopoly: Pokemon', 5, 'Partner with Pikachu and friends in Pokemon Monopoly! Travel through all eight gyms and battle all kinds of Pokemon in the Pokemon Kanto Edition of Monopoly. Buy, sell and trade with other trainers to collect the most powerful Pokemon team!', 49.99, 7, 26, '2018-05-11 08:46:33', 0),
(26, 'Uno', 5, 'Easy to pick up... impossible to put down!', 12.99, 4, 27, '2018-05-11 08:46:33', 0),
(27, 'The Way', 4, 'Most Valued Magic Card', 999999.99, 1, 29, '2018-05-13 07:34:02', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `orderID` int(8) NOT NULL,
  `itemID` int(8) NOT NULL,
  `customerID` int(8) NOT NULL,
  `transactionID` int(8) NOT NULL,
  `price` double(8,2) NOT NULL,
  `quantity` int(2) NOT NULL,
  `orderType` enum('boxbreak','item') NOT NULL,
  `shipDate` date NOT NULL,
  `shipAddress` varchar(255) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productimage`
--

CREATE TABLE `productimage` (
  `imageID` int(8) NOT NULL,
  `imagePath` varchar(255) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productimage`
--

INSERT INTO `productimage` (`imageID`, `imagePath`, `dateAdded`, `deleted`) VALUES
(14, 'image/redeyes.jpg', '2018-05-11 08:31:20', 0),
(15, 'image/blueeyes.jpg', '2018-05-11 08:31:20', 0),
(16, 'image/darkmagician.jpg', '2018-05-11 08:31:20', 0),
(17, 'image/charizard.jpg', '2018-05-11 08:31:20', 0),
(18, 'image/blastoise.jpg', '2018-05-11 08:31:20', 0),
(19, 'image/venusaur.jpg', '2018-05-11 08:31:20', 0),
(20, 'image/jace.jpg', '2018-05-11 08:31:20', 0),
(21, 'image/ulamog.jpg', '2018-05-11 08:31:20', 0),
(22, 'image/blacklotus.jpg', '2018-05-11 08:31:20', 0),
(23, 'image/thor.jpg', '2018-05-11 08:31:20', 0),
(24, 'image/spiderman.jpg', '2018-05-11 08:31:20', 0),
(25, 'image/doctorstrange.jpg', '2018-05-11 08:31:20', 0),
(26, 'image/monopoly.jpg', '2018-05-11 08:31:20', 0),
(27, 'image/uno.jpg', '2018-05-11 08:31:20', 0),
(28, 'image/clue.jpg', '2018-05-11 08:31:20', 0),
(29, 'image/1515546711308176.png', '2018-05-13 07:34:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `teamID` int(8) NOT NULL,
  `sport` varchar(36) NOT NULL,
  `city` varchar(72) NOT NULL,
  `team` varchar(72) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`teamID`, `sport`, `city`, `team`) VALUES
(2, 'hockey', 'Carolina', 'Hurricanes'),
(3, 'hockey', 'Colombus', 'Blue Jackets'),
(4, 'hockey', 'New Jersey', 'Devils'),
(5, 'hockey', 'New York', 'Islanders'),
(6, 'hockey', 'New York', 'Rangers'),
(7, 'hockey', 'Philadelphia', 'Flyers'),
(8, 'hockey', 'Pittsburgh', 'Penguins'),
(9, 'hockey', 'Washington', 'Capitals'),
(10, 'hockey', 'Boston', 'Bruins'),
(11, 'hockey', 'Buffalo', 'Sabres'),
(12, 'hockey', 'Detroit', 'Red Wings'),
(13, 'hockey', 'Florida', 'Panthers'),
(14, 'hockey', 'Montreal', 'Canadiens'),
(15, 'hockey', 'Ottawa', 'Senators'),
(16, 'hockey', 'Tampa Bay', 'Lightning'),
(17, 'hockey', 'Toronto', 'Maple Leafs'),
(18, 'hockey', 'Chicago', 'Blackhawks'),
(19, 'hockey', 'Colorado', 'Avalanche'),
(20, 'hockey', 'Dallas', 'Stars'),
(21, 'hockey', 'Minnesota', 'Wild'),
(22, 'hockey', 'Nashville', 'Predators'),
(23, 'hockey', 'St. Louis', 'Blues'),
(24, 'hockey', 'Winnipeg', 'Jets'),
(25, 'hockey', 'Anaheim', 'Ducks'),
(26, 'hockey', 'Arizona', 'Coyotes'),
(27, 'hockey', 'Calgary', 'Flames'),
(28, 'hockey', 'Edmonton', 'Oilers'),
(29, 'hockey', 'Los Angeles', 'Kings'),
(30, 'hockey', 'San Jose', 'Sharks'),
(31, 'hockey', 'Vancouver', 'Canucks'),
(32, 'hockey', 'Vegas', 'Golden Knights'),
(33, 'baseball', 'New York', 'Yankees'),
(34, 'baseball', 'Boston', 'Red Sox'),
(35, 'baseball', 'Chicago', 'White Sox'),
(36, 'baseball', 'Cleveland', 'Indians'),
(37, 'baseball', 'Detroit', 'Tigers'),
(38, 'baseball', 'Baltimore', 'Orioles'),
(39, 'baseball', 'Oakland', 'Athletics'),
(40, 'baseball', 'Minnesota', 'Twins'),
(41, 'baseball', 'Tampa Bay', 'Rays'),
(42, 'baseball', 'Toronto', 'Blue Jays'),
(43, 'baseball', 'Kansas City', 'Royals'),
(44, 'baseball', 'Houston', 'Astros'),
(45, 'baseball', 'Los Angeles', 'Angels of Anaheim'),
(46, 'baseball', 'Seattle', 'Mariners'),
(47, 'baseball', 'Texas', 'Rangers'),
(48, 'baseball', 'Atlanta', 'Braves'),
(49, 'baseball', 'Los Angeles', 'Dodgers'),
(50, 'baseball', 'New York', 'Mets'),
(51, 'baseball', 'Montreal', 'Expos'),
(52, 'baseball', 'San Diego', 'Padres'),
(53, 'baseball', 'Colorado', 'Rockies'),
(54, 'baseball', 'Miami', 'Marlins'),
(55, 'baseball', 'Arizona', 'Diamondbacks'),
(56, 'baseball', 'Milwaukee', 'Brewers'),
(57, 'baseball', 'Washington', 'Nationals'),
(58, 'baseball', 'Chicago', 'Cubs'),
(59, 'baseball', 'Cincinnati', 'Reds'),
(60, 'baseball', 'Pittsburgh', 'Pirates'),
(61, 'baseball', 'St. Louis', 'Cardinals'),
(62, 'baseball', 'San Francisco', 'Giants'),
(63, 'baseball', 'Philadelphia', 'Phillies'),
(64, 'football', 'Buffalo', 'Bills'),
(65, 'football', 'Miami', 'Dolphins'),
(66, 'football', 'New England', 'Patriots'),
(67, 'football', 'New York', 'Jets'),
(68, 'football', 'Baltimore', 'Ravens'),
(69, 'football', 'Cincinnati', 'Bengals'),
(70, 'football', 'Cleveland', 'Browns'),
(71, 'football', 'Pittsburgh', 'Steelers'),
(72, 'football', 'Houston', 'Texans'),
(73, 'football', 'Indianapolis', 'Colts'),
(74, 'football', 'Jacksonville', 'Cougars'),
(75, 'football', 'Tennessee', 'Titans'),
(76, 'football', 'Denver', 'Broncos'),
(77, 'football', 'Kansas City', 'Chiefs'),
(78, 'football', 'Los Angeles', 'Chargers'),
(79, 'football', 'Oakland', 'Raiders'),
(80, 'football', 'Dallas', 'Cowboys'),
(81, 'football', 'New York', 'Giants'),
(82, 'football', 'Philadelphia', 'Eagles'),
(83, 'football', 'Washington', 'Redskins'),
(84, 'football', 'Chicago', 'Bears'),
(85, 'football', 'Detroit', 'Lions'),
(86, 'football', 'Green Bay', 'Packers'),
(87, 'football', 'Minnesota', 'Vikings'),
(88, 'football', 'Atlanta', 'Falcons'),
(89, 'football', 'Carolina', 'Panthers'),
(90, 'football', 'New Orleans', 'Saints'),
(91, 'football', 'Tampa Bay', 'Buccaneers'),
(92, 'football', 'Arizona', 'Cardinals'),
(93, 'football', 'Los Angeles', 'Rams'),
(94, 'football', 'San Francisco', '49ers'),
(95, 'football', 'Seattle', 'Seahawks');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `transactionID` int(8) NOT NULL,
  `paymentType` varchar(2) NOT NULL,
  `paymentAmount` double(8,2) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boxbreak`
--
ALTER TABLE `boxbreak`
  ADD PRIMARY KEY (`itemID`),
  ADD KEY `breakID` (`itemID`),
  ADD KEY `teamID` (`teamID`),
  ADD KEY `customerID` (`customerID`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryID`),
  ADD KEY `categoryID` (`categoryID`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerID`),
  ADD KEY `customerID` (`customerID`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`itemID`),
  ADD KEY `itemID` (`itemID`),
  ADD KEY `imageID` (`imageID`),
  ADD KEY `categoryID` (`categoryID`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`orderID`),
  ADD KEY `orderID` (`orderID`),
  ADD KEY `itemID` (`itemID`),
  ADD KEY `customerID` (`customerID`),
  ADD KEY `transactionID` (`transactionID`);

--
-- Indexes for table `productimage`
--
ALTER TABLE `productimage`
  ADD PRIMARY KEY (`imageID`),
  ADD KEY `imageID` (`imageID`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`teamID`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transactionID`),
  ADD KEY `transactionID` (`transactionID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boxbreak`
--
ALTER TABLE `boxbreak`
  MODIFY `itemID` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `categoryID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customerID` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `itemID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `orderID` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productimage`
--
ALTER TABLE `productimage`
  MODIFY `imageID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `teamID` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `transactionID` int(8) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `boxbreak`
--
ALTER TABLE `boxbreak`
  ADD CONSTRAINT `boxbreak_ibfk_2` FOREIGN KEY (`teamID`) REFERENCES `team` (`teamID`),
  ADD CONSTRAINT `boxbreak_ibfk_4` FOREIGN KEY (`customerID`) REFERENCES `customer` (`customerID`);

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`imageID`) REFERENCES `productimage` (`imageID`),
  ADD CONSTRAINT `item_ibfk_2` FOREIGN KEY (`categoryID`) REFERENCES `category` (`categoryID`);

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`itemID`) REFERENCES `item` (`itemID`),
  ADD CONSTRAINT `order_ibfk_2` FOREIGN KEY (`customerID`) REFERENCES `customer` (`customerID`),
  ADD CONSTRAINT `order_ibfk_3` FOREIGN KEY (`transactionID`) REFERENCES `transaction` (`transactionID`),
  ADD CONSTRAINT `order_ibfk_4` FOREIGN KEY (`itemID`) REFERENCES `boxbreak` (`itemID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
